import Vue from 'vue'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import VueRouter from 'vue-router'
import Routes from './routes'
import auth from './auth'
import axios from 'axios'
import tokenProvider from 'axios-token-interceptor'
import infiniteScroll from 'vue-infinite-scroll'


if ( auth.isLoggedIn() ) {
  auth.user.authenticated = true;
  auth.user.username = localStorage.getItem('username');
}

axios.interceptors.request.use( async (config) => {
    const access_token = await auth.getNewAccessToken();
    if (access_token) {
      config.headers.common['Authorization'] = 'Bearer ' + access_token;
    }
    return config;
  }, (error) => {
    return Promise.reject(error);
  })

Vue.use(VueRouter);
const router = new VueRouter({
  routes: Routes,
  mode: 'history'
});

Vue.use(BootstrapVue);
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(infiniteScroll);

new Vue({
  el: '#app',
  render: h => h(App),
  router: router,
})
