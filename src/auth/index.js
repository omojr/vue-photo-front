import decode from 'jwt-decode'
import axios from 'axios'

const auth_axios = axios.create();
auth_axios.interceptors.request.use( (config) => {
  config.headers['Content-Type'] = 'application/json'
  return config;
})

const API_URL = 'http://sec.omo.in.ua:8000/';
const LOGIN_URL = API_URL + 'token/';
const REFRESH_URL = LOGIN_URL + 'refresh/';

export default {

  user: {
    authenticated: false,
    username: ''
  },

  login(context, data, redirect) {
    return new Promise( (resolve, reject) => {
      console.log('auth login');
      auth_axios.post(LOGIN_URL, data).then(response => {
        var resp_data = response.data;
        localStorage.setItem('refresh_token', resp_data.refresh);
        localStorage.setItem('access_token', resp_data.access);
        localStorage.setItem('username', data.username);

        this.user.authenticated = true;
        this.user.username = data.username;

        if(redirect) {
          context.$router.push(redirect);
        } else {
          context.$router.push('/');
        }
      }).catch(e => {
        resolve(e);
      });
    })
  },

  logout(context) {
    localStorage.removeItem('refresh_token');
    localStorage.removeItem('access_token');
    localStorage.removeItem('username');
    this.user.authenticated = false;
    this.user.username = '';
    context.$router.push('/login');
  },

  isLoggedIn() {
    const access_token = localStorage.getItem('access_token');
    return access_token && !this.isTokenExpired(access_token);
  },

  parseJwt(token) {
      var base64Url = token.split('.')[1];
      var base64 = base64Url.replace('-', '+').replace('_', '/');
      return JSON.parse(window.atob(base64));
  },

  getTokenExpirationDate(encodedToken) {
    const token = this.parseJwt(encodedToken);
    if (!token.exp) {
      return null; 
    }
    const date = new Date(0);
    date.setUTCSeconds(token.exp);
    return date;
  },

  isTokenExpired(access_token) {
    const expirationDate = this.getTokenExpirationDate(access_token);
    return expirationDate < new Date();
  },

  getNewAccessToken() {
    return new Promise( (resolve, reject) => {
      const accessToken = localStorage.getItem('access_token');
      if (!accessToken || this.isTokenExpired(accessToken)) {
        const refreshToken = localStorage.getItem('refresh_token');
        if (refreshToken && !this.isTokenExpired(refreshToken)) {
          var post_data = {refresh: refreshToken};
          auth_axios.post(REFRESH_URL, JSON.stringify(post_data)).then( response => {
            localStorage.setItem('access_token', response.data.access);
            resolve(response.data.access);
          }).catch( error => {
            console.log(error);
            resolve("");
          })
        } else {
          resolve("")
        }
      } else {
        resolve(accessToken);
      }
    })
  },
}