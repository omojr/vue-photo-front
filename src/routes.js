// import ListUser from './components/ListUser.vue'
import NewUser from './components/NewUser.vue'
import PhotoNew from './components/PhotoNew.vue'
import PhotoList from './components/PhotoList.vue'
import PhotoView from './components/PhotoView.vue'
import Login from './components/Login.vue'
import Main from './components/Main.vue'

export default [
  {path: '/', component: Main},
  // {path: '/users', component: ListUser},
  {path: '/register', component: NewUser},
  {path: '/upload', component: PhotoNew},
  // {path: '/feed', component: PhotoList},
  {path: '/photos', component: PhotoList, alias: '/feed'},
  {path: '/photos/:id', name: "PhotoView", component: PhotoView},
  {path: '/login', component: Login},
]